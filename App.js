import React from 'react';
import { Text,View } from 'react-native';
import { Provider } from 'react-redux'
import { createStore } from "redux";
import reducers from './src/reducers';
import Header from './src/components/common/Header';
import TechList from './src/components/TechList'

export default class App extends React.Component {
  render() {
    return (
      <Provider store={createStore(reducers)}>
        <View style={{flex:1}}>
          <Header text='Technical Stack' />
          <TechList />
        </View>
      </Provider>
    );
  }
}
