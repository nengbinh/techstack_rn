export const selectTech = (techID) => {
    return {
        type: 'select_tech',
        payload: techID
    }
}
