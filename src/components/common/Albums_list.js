import React from 'react'
import {ScrollView,Text,View} from 'react-native'
import Albums_detail from './Albums_detail'

class AlbumsList extends React.Component{
    state = { albums:[] };

    // this is a lifecycle methods will just run it once
    componentWillMount() {
        fetch("https://albums-app-data-p.oss-us-west-1.aliyuncs.com/data.json")
            .then(response => response.json() )
            .then( responseJson =>  this.setState({albums:responseJson}))
    }

    renderDetails() {
        return this.state.albums.map(album => <Albums_detail key={album.title} album={album} />)
    }

    render() {
        console.log(this.state)
        return (
            <ScrollView>
                {this.renderDetails()}
                <View style={{paddingBottom: 8}} />
            </ScrollView>
        )
    }
}

export default AlbumsList;
