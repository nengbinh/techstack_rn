// 1. import React and its relevant modules
import React from 'react'
import {Text, View} from 'react-native'

// 2. create header component
const Header = props => {
    // this is ES6 syntactic sugar
    const { textStyle,viewStyle } = styles;
    // Same as below
    // const textStyle = styles.textStyle;

    return (
        <View style={viewStyle}>
            <Text style={textStyle}>{props.text}</Text>
        </View>
    )
}

// 3. export header component
export default Header

// this is styling
const styles = {
    textStyle: {
        fontSize: 16
    },
    viewStyle: {
        backgroundColor: '#E0E0E0',
        height: 64,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
        shadowColor: '#000',
        shadowOffset: {width:0, height: 2},
        shadowOpacity: 0.3,
        elevation: 2,
        position: 'relative'
    }
}
