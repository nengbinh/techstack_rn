import React from 'react'
import {Text, TouchableWithoutFeedback, View, LayoutAnimation} from 'react-native'
import CardSelection from './common/CardSelection'
import * as actions from './action/index'
import {connect} from 'react-redux'

class ListItem extends React.Component{
    componentWillUpdate(nextProps, nextState, nextContext) {
        LayoutAnimation.spring()
    }

    renderDescription(description) {
        if (this.props.selected) {
            return (
                <CardSelection>
                    <Text style={styles.textStyle}>
                        {description}
                    </Text>
                </CardSelection>
            )
        }
    }
    //
    render() {
        const {titleStyle} = styles;
        const {title,id,description} = this.props.tech;
        //
        return (
            <TouchableWithoutFeedback onPress={()=>this.props.selectTech(id)}>
                <View>
                    <CardSelection>
                        <Text style={titleStyle}>{title}</Text>
                    </CardSelection>
                    {this.renderDescription(description)}
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = {
    titleStyle: {
        fontSize: 18
    },
    textStyle: {
        textAlign: 'justify'
    }
};

const mapStateToProps = (state, ownProps) => {
    return { selected: ownProps.tech.id === state.selectTechId }
}

export default connect(mapStateToProps, actions)(ListItem)
