import {combineReducers} from "redux";
import TechReducer from './TechReducers'
import SelectionReducers from './SelectionReducers'

export default combineReducers({
    techs: TechReducer,
    selectTechId: SelectionReducers
})
